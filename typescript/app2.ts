import Carro from './Carro';
import Concessionaria from './Concessionaria';
import Pessoa from './Pessoa';

// Cria carros
let carroA = new Carro("Astra", 4);
let carroB = new Carro("HB20", 2);
let carroC = new Carro("BMW X1 20i", 4);

// Montar lista de carros
let listCarros: Array<Carro> = [carroA, carroB, carroC];
let concessionaria = new Concessionaria("Rua Geraldo", listCarros);

// Exibir lista de carros
// console.log(concessionaria.mostrarListaDeCarros())

// comprar carro
let cliente = new Pessoa("Joao", "BMW X1 20i");

concessionaria.mostrarListaDeCarros().map((carro: Carro) => {
    if(carro['modelo'] == cliente.dizerCarroPreferido()) {
        
        //comprar o carro
        cliente.comprarCarro(carro);
    }
})

console.log(cliente.dizerCarroQueTem());