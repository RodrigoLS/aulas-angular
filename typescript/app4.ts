// import { ConcessionariaDao } from './ConcessionariaDao';
// import { PessoaDao } from './PessoaDao';
import Concessionaria from './Concessionaria';
import Pessoa from './Pessoa';
import { Dao } from './Dao';


// let dao: ConcessionariaDao = new ConcessionariaDao();
// let dao2: PessoaDao = new PessoaDao();

// dao.inserir(concessionaria);
// dao2.atualizar(pessoa);

let concessionaria = new Concessionaria("", []);
let pessoa = new Pessoa("Alfredo", "Palho");

let dao3: Dao<Concessionaria> = new Dao<Concessionaria>();
let dao4: Dao<Pessoa> = new Dao<Pessoa>();

dao3.inserir(concessionaria);
dao4.inserir(pessoa);