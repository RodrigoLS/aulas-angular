import { DaoInterface } from './DaoInterface';
import Pessoa from './Pessoa';

export class PessoaDao implements DaoInterface {
    nomeTabela: string = "tb_pessoa";

    inserir(object: Pessoa): boolean {
        console.log("lógica insert");
        return true;
    }

    atualizar(object: Pessoa): boolean {
        console.log("lógica update");
        return true;
    }

    remover(id: number): Pessoa {
        console.log("lógica delete");
        return new Pessoa("João", "Celta");
    }

    selecionar(id: number): Pessoa {
        console.log("lógica select");
        return new Pessoa("Maria", "Uno");
    }

    selecionarTodos(): Array<Pessoa> {
        "lógica select All";
        return [new Pessoa("Juninho", "Gol Quadrado")];
    }
}