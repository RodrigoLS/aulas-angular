"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Pessoa_1 = __importDefault(require("./Pessoa"));
var PessoaDao = /** @class */ (function () {
    function PessoaDao() {
        this.nomeTabela = "tb_pessoa";
    }
    PessoaDao.prototype.inserir = function (object) {
        console.log("lógica insert");
        return true;
    };
    PessoaDao.prototype.atualizar = function (object) {
        console.log("lógica update");
        return true;
    };
    PessoaDao.prototype.remover = function (id) {
        console.log("lógica delete");
        return new Pessoa_1.default("João", "Celta");
    };
    PessoaDao.prototype.selecionar = function (id) {
        console.log("lógica select");
        return new Pessoa_1.default("Maria", "Uno");
    };
    PessoaDao.prototype.selecionarTodos = function () {
        "lógica select All";
        return [new Pessoa_1.default("Juninho", "Gol Quadrado")];
    };
    return PessoaDao;
}());
exports.PessoaDao = PessoaDao;
