"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var Carro_1 = __importDefault(require("./Carro"));
var Concessionaria_1 = __importDefault(require("./Concessionaria"));
var Pessoa_1 = __importDefault(require("./Pessoa"));
// Cria carros
var carroA = new Carro_1.default("Astra", 4);
var carroB = new Carro_1.default("HB20", 2);
var carroC = new Carro_1.default("BMW X1 20i", 4);
// Montar lista de carros
var listCarros = [carroA, carroB, carroC];
var concessionaria = new Concessionaria_1.default("Rua Geraldo", listCarros);
// Exibir lista de carros
// console.log(concessionaria.mostrarListaDeCarros())
// comprar carro
var cliente = new Pessoa_1.default("Joao", "BMW X1 20i");
concessionaria.mostrarListaDeCarros().map(function (carro) {
    if (carro['modelo'] == cliente.dizerCarroPreferido()) {
        //comprar o carro
        cliente.comprarCarro(carro);
    }
});
console.log(cliente.dizerCarroQueTem());
