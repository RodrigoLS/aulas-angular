import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RestauranteComponent } from './restaurante/restaurante.component';
import { DiversaoComponent } from './diversao/diversao.component';
import { OfertaComponent } from 'src/app/oferta/oferta.component';
import { ComoUsarComponent } from 'src/app/oferta/como-usar/como-usar.component';
import { OndeFicaComponent } from 'src/app/oferta/onde-fica/onde-fica.component';

export const ROUTES: Routes = [
    { path: '', component: HomeComponent },
    { path: 'restaurante', component: RestauranteComponent },
    { path: 'diversao', component: DiversaoComponent },
    { path: 'oferta', component: HomeComponent },
    { path: 'oferta/:id', component: OfertaComponent,
        children: [
            {path: '', component: ComoUsarComponent},
            {path: 'como-usar', component: ComoUsarComponent},
            {path: 'onde-fica', component: OndeFicaComponent},
        ]
    }
]