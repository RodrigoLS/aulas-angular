import { Oferta } from './shared/oferta.model';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { URL_API } from './app.api';
// import 'rxjs/add/operator/toPromise';

@Injectable()

export class OfertasService {

    constructor(private http: Http) {

    }

    public getOfertas(): Promise<Oferta[]> {
        //efetuar uma requisição http
        //retornar um promise Oferta[]
        return this.http.get(`${URL_API}?destaque=true`)
        .toPromise()
        .then((resposta: any) => resposta.json())
    }

    public getOfertasPorCategoria(categoria: string): Promise<Oferta[]> {
        return this.http.get(`${URL_API}?categoria=${categoria}`)
        .toPromise()
        .then((resposta: any) => resposta.json())
    }

    public getOferta(id: string): Promise<Oferta> {
        return this.http.get(`${URL_API}?id=${id}`)
        .toPromise()
        .then((resposta: any) => resposta.json()[0])
    }
}